package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserDataBeans;


/**
 * ユーザテーブル用のDao
 *
 *
 */
public class UserDao {

    /**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     * @param loginId
     * @param password
     * @return
     */
    public UserDataBeans findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM t_user WHERE login_id = ? and login_password = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, password);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            int idData = rs.getInt("id");

            return new UserDataBeans(loginIdData, nameData,idData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }




    /**
	 * 顧客情報を登録する処理
	 */
    /**
	 * データの挿入処理を行う。現在時刻は挿入直前に生成
	 *
	 * @param user
	 *            対応したデータを保持しているJavaBeans
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためにスロー
	 */
	public static void insertUser(UserDataBeans udb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO t_user(name,login_id,address,login_password,create_date) VALUES(?,?,?,?, NOW())");
			st.setString(1, udb.getName());
			st.setString(2, udb.getLoginId());
			st.setString(3, udb.getAddress());
			st.setString(4, udb.getPassword());

			st.executeUpdate();
			System.out.println("inserting user has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}



	/**
	 * ユーザー情報詳細処理
	 */

public UserDataBeans findById(int id) {
    Connection conn = null;
    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql = "SELECT * FROM t_user WHERE id = ? ";

         // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setInt(1, id);

        ResultSet rs = pStmt.executeQuery();


        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        if (!rs.next()) {
            return null;
        }

        // 必要なデータのみインスタンスのフィールドに追加
        int IdData = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        String address = rs.getString("address");



        return new UserDataBeans(name,address,loginId,IdData);

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
  }


/**
 * 顧客情報を更新する処理
 */
public void updateUser(String name, String login_id, String address, int id) {
	Connection con = null;
	PreparedStatement stmt = null;

	try {
		// データベース接続
		con = DBManager.getConnection();

		// 実行SQL文字列定義
		String insertSQL = "UPDATE t_user SET name=?, login_id=?, address=? WHERE id=?";

		// ステートメント生成
		stmt = con.prepareStatement(insertSQL);

		// SQLの?パラメータに値を設定

		stmt.setString(1, name);
		stmt.setString(2, login_id);
		stmt.setString(3, address);
		stmt.setInt(4, id);



		// 登録SQL実行
		stmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		try {
			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {
				stmt.close();
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
/**
 * 顧客情報を削除する処理
 */
public void delUser(int id) {
	Connection con = null;
	PreparedStatement stmt = null;

	try {
		// データベース接続
		con = DBManager.getConnection();

		// 実行SQL文字列定義
		String insertSQL = "DELETE  FROM user WHERE id = ? ";

		// ステートメント生成
		stmt = con.prepareStatement(insertSQL);

		// SQLの?パラメータに値を設定

		stmt.setInt(1, id);

		// 登録SQL実行
		stmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		try {
			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {
				stmt.close();
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
/**
 * ログインID詳細処理
 */

//public User findByUserId(String loginId) {
//Connection conn = null;
//try {
//    // データベースへ接続
//    conn = DBManager.getConnection();
//
//    // SELECT文を準備
//    String sql = "SELECT * FROM user WHERE login_id = ? ";
//
//     // SELECTを実行し、結果表を取得
//    PreparedStatement pStmt = conn.prepareStatement(sql);
//    pStmt.setString(1, loginId);
//
//    ResultSet rs = pStmt.executeQuery();
//
//
//    // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
//    if (!rs.next()) {
//        return null;
//    }
//
//    // 必要なデータのみインスタンスのフィールドに追加
//    int IdData = rs.getInt("id");
//    String loginid = rs.getString("login_id");
//    String name = rs.getString("name");
//    Date birthDate = rs.getDate("birth_date");
//    String password = rs.getString("password");
//    String createDate = rs.getString("create_date");
//    String updateDate = rs.getString("update_date");
//
//
//    return new User(IdData,loginid, name, birthDate, password, createDate, updateDate);
//
//} catch (SQLException e) {
//    e.printStackTrace();
//    return null;
//} finally {
//    // データベース切断
//    if (conn != null) {
//        try {
//            conn.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//}
//}

/**
 * 暗号化するメソッド
 */
	public String pass(String password) {


		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes;

		String result = "";

		try {
			bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
			 result = DatatypeConverter.printHexBinary(bytes);



		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return result;

	}

	 /**
     * ユーザ情報を検索する
     * @return
     */
//    public List<User> findloginIdSearch(String loginId, String name, String birthDate ,String birthDate2) {
//        Connection conn = null;
//        List<User> userList = new ArrayList<User>();
//
//        try {
//            // データベースへ接続
//            conn = DBManager.getConnection();
//
//            // SELECT文を準備
//
//            String sql = "SELECT * FROM user WHERE name not in ('管理者')";
//
//            if (!loginId.equals("")) {
//            	sql += " AND login_id = '" + loginId + "'";
//			}
//
//	          if (!name.equals("")) {
//	        	sql += "AND name LIKE '%" + name + "%'";
//			}
//
//	          if (!birthDate.equals("")) {
//	            sql += "AND birth_date >= '" + birthDate + "'";
//			}
//
//	          if(!birthDate2.equals("")) {
//	        	  sql += "AND birth_date <= '" + birthDate2 + "'";
//	          }
//
//	            // SELECTを実行し、結果表を取得
//	            Statement stmt = conn.createStatement();
//	            ResultSet rs = stmt.executeQuery(sql);
//
//            while (rs.next()) {
//                int id = rs.getInt("id");
//                String loginID = rs.getString("login_id");
//                String NAME = rs.getString("name");
//                Date BirthDate = rs.getDate("birth_date");
//                String password = rs.getString("password");
//                String createDate = rs.getString("create_date");
//                String updateDate = rs.getString("update_date");
//                User user = new User(id, loginID, NAME, BirthDate, password, createDate, updateDate);
//
//                userList.add(user);
//
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//            return null;
//        } finally {
//            // データベース切断
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                    return null;
//                }
//            }
//        }
//        return userList;
//    }

}

