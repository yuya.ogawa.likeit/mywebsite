package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ItemDataBeans;

public class ItemMasterDao {

	 /**
		 * アイテムを登録する処理
		 */
		public void createItem(String name, String detail, int price, String file_name) {
			Connection con = null;
			PreparedStatement stmt = null;

			try {
				// データベース接続
				con = DBManager.getConnection();

				// 実行SQL文字列定義
				String insertSQL = "INSERT INTO m_item(name,detail,price,file_name) VALUES(?,?,?,?);";

				// ステートメント生成
				stmt = con.prepareStatement(insertSQL);

				// SQLの?パラメータに値を設定

				stmt.setString(1, name);
				stmt.setString(2, detail);
				stmt.setInt(3, price);
				stmt.setString(4, file_name);


				// 登録SQL実行
				stmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					// ステートメントインスタンスがnullでない場合、クローズ処理を実行
					if (stmt != null) {
						stmt.close();
					}
					// コネクションインスタンスがnullでない場合、クローズ処理を実行
					if (con != null) {
						con.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		/**
	     * 全てのアイテム情報を取得する
	     * @return
	     */
	    public List<ItemDataBeans> findAll() {
	        Connection conn = null;
	        List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備

	            String sql = "SELECT * FROM m_item";

	             // SELECTを実行し、結果表を取得
	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

	            // 結果表に格納されたレコードの内容を
	            // Userインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String name = rs.getString("name");
	                String detail = rs.getString("detail");
	                int price = rs.getInt("price");
	                String fileName = rs.getString("file_name");


	                ItemDataBeans item = new ItemDataBeans(id, name, detail, price,fileName);

	                itemList.add(item);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return itemList;
	    }

	    /**
		 * アイテム情報詳細処理
		 */

	public static ItemDataBeans findById(int id) {
	    Connection conn = null;
	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();

	        // SELECT文を準備
	        String sql = "SELECT * FROM m_item WHERE id = ? ";

	         // SELECTを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setInt(1, id);

	        ResultSet rs = pStmt.executeQuery();


	        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	        if (!rs.next()) {
	            return null;
	        }

	        // 必要なデータのみインスタンスのフィールドに追加
	        int idData = rs.getInt("id");
            String name = rs.getString("name");
            String detail = rs.getString("detail");
            int price = rs.getInt("price");
            String fileName = rs.getString("file_name");


	        return new ItemDataBeans(idData,name, detail, price, fileName);

	    } catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
	    }
	  }

	/**
	 * アイテム情報を削除する処理
	 */
	public void delItem(int id) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();

			// 実行SQL文字列定義
			String insertSQL = "DELETE  FROM m_item WHERE id = ? ";

			// ステートメント生成
			stmt = con.prepareStatement(insertSQL);

			// SQLの?パラメータに値を設定

			stmt.setInt(1, id);

			// 登録SQL実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * アイテム情報を更新する処理
	 */
	public void updateItem(String name, String detail, int price, String file_name, int id) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();

			// 実行SQL文字列定義
			String insertSQL = "UPDATE m_item SET name = ?, detail = ?, price = ?, file_name = ? WHERE id = ?";

			// ステートメント生成
			stmt = con.prepareStatement(insertSQL);

			// SQLの?パラメータに値を設定

			stmt.setString(1, name);
			stmt.setString(2, detail);
			stmt.setInt(3, price);
			stmt.setString(4, file_name);
			stmt.setInt(5, id);



			// 登録SQL実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
     * Item情報を検索する
     * @return
     */
    public List<ItemDataBeans> findItemNameSearch(String name) {
        Connection conn = null;
        List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備

            String sql = "SELECT * FROM m_item WHERE name LIKE '%" + name + "%'";



	            // SELECTを実行し、結果表を取得
	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
            	 // 必要なデータのみインスタンスのフィールドに追加
    	        int idData = rs.getInt("id");
                String Name = rs.getString("name");
                String detail = rs.getString("detail");
                int price = rs.getInt("price");
                String fileName = rs.getString("file_name");

                ItemDataBeans item = new ItemDataBeans(idData, Name, detail, price, fileName);

                itemList.add(item);

            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return itemList;
    }

}
