package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemDataBeans;
import dao.ItemMasterDao;

/**
 * Servlet implementation class ItemMasterDetails
 */
@WebServlet("/ItemMasterDetails")
public class ItemMasterDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemMasterDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// マルチバイト文字列の文字化け対応
		request.setCharacterEncoding("UTF-8");

		// URLパラメータから値を取得
		String userID = request.getParameter("id");
		int id = Integer.parseInt(userID);

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		ItemMasterDao ItemMasterDao = new ItemMasterDao();
		ItemDataBeans idb = ItemMasterDao.findById(id);

		// リクエスト領域に値をセット
		request.setAttribute("idb",idb );


		// フォワードだよ
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemMasterdetails.jsp");
				dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {



	}

}
