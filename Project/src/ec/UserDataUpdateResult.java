package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserDataUpdateResult
 */
@WebServlet("/UserDataUpdateResult")
public class UserDataUpdateResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDataUpdateResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		// セッションにユーザの情報をセット
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		String inputUserName = request.getParameter("user_name");
		String inputUserAddress = request.getParameter("user_address");
		String inputLoginId = request.getParameter("login_id");

		int id = user.getId();

		// 値をセット
					request.setAttribute("login_id",inputLoginId );
					request.setAttribute("user_name",inputUserName );
					request.setAttribute("user_address",inputUserAddress );
					request.setAttribute("id",id );

					 // 、Daoのメソッドを実行
			     	UserDao userDao = new UserDao();

			     	 // 、Daoの更新メソッドを実行
			     	userDao.updateUser(inputUserName,inputLoginId,inputUserAddress, id);


		// フォワードだよ
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdataupdateresult.jsp");
				dispatcher.forward(request, response);

	}

}
