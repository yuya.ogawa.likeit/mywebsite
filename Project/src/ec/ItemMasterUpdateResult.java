package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemDataBeans;
import dao.ItemMasterDao;

/**
 * Servlet implementation class ItemMasterUpdateResult
 */
@WebServlet("/ItemMasterUpdateResult")
public class ItemMasterUpdateResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemMasterUpdateResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");

		String inputItemName = request.getParameter("item_name");
		String inputItemPrice = request.getParameter("item_price");
		String inputItemDetail = request.getParameter("item_detail");
		  String name = request.getParameter("file");
		  String inputItemId = request.getParameter("id");

		  ItemDataBeans idb = new ItemDataBeans();
			idb.setName(inputItemName);
			idb.setPrice(Integer.parseInt(inputItemPrice));
			idb.setDetail(inputItemDetail);
			idb.setFileName(name);
			idb.setId(Integer.parseInt(inputItemId));

			request.setAttribute("idb", idb);

			 // 、Daoのメソッドを実行
	     	ItemMasterDao ItemMasterDao = new ItemMasterDao();

	     	 // 、Daoの更新メソッドを実行
	     	ItemMasterDao.updateItem(inputItemName,inputItemDetail,Integer.parseInt(inputItemPrice),name,Integer.parseInt(inputItemId));


			// フォワードだよ
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemMasterupdateresult.jsp");
			dispatcher.forward(request, response);


	}

}
