package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemDataBeans;
import dao.ItemMasterDao;

/**
 * Servlet implementation class ItemMasterIndex
 */
@WebServlet("/ItemMasterIndex")
public class ItemMasterIndex extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemMasterIndex() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// アイテム一覧情報を取得
				ItemMasterDao ItemMasterDao = new ItemMasterDao();
				List<ItemDataBeans> itemList = ItemMasterDao.findAll();

				// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("itemList", itemList);

		// フォワードだよ
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemMasterindex.jsp");
				dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {



	}

}
