package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import beans.ItemDataBeans;

/**
 * Servlet implementation class ItemMasterRegist
 */
@WebServlet("/ItemMasterRegist")
@MultipartConfig(location="/Users/ogawayuuya/MyWebSite/Project/WebContent/img", maxFileSize=1048576)
public class ItemMasterRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemMasterRegist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// フォワードだよ
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemMasterregist.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");

		String inputItemName = request.getParameter("item_name");
		String inputItemPrice = request.getParameter("item_price");
		String inputItemDetail = request.getParameter("item_detail");

		  Part part = request.getPart("file");
		  String name = this.getFileName(part);
		  part.write(name);

		  ItemDataBeans idb = new ItemDataBeans();
			idb.setName(inputItemName);
			idb.setPrice(Integer.parseInt(inputItemPrice));
			idb.setDetail(inputItemDetail);
			idb.setFileName(name);

			request.setAttribute("idb", idb);


			// フォワードだよ
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemMasterregistconfirm.jsp");
			dispatcher.forward(request, response);

	}

    private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
    }
}
