package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import beans.ItemDataBeans;
import dao.ItemMasterDao;

/**
 * Servlet implementation class ItemMasterUpdate
 */
@WebServlet("/ItemMasterUpdate")
@MultipartConfig(location="/Users/ogawayuuya/MyWebSite/Project/WebContent/img", maxFileSize=1048576)
public class ItemMasterUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemMasterUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// マルチバイト文字列の文字化け対応
				request.setCharacterEncoding("UTF-8");

				// URLパラメータから値を取得
				String userID = request.getParameter("id");
				int id = Integer.parseInt(userID);

				// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
				ItemMasterDao ItemMasterDao = new ItemMasterDao();
				ItemDataBeans idb = ItemMasterDao.findById(id);

				// リクエスト領域に値をセット
				request.setAttribute("item_name",idb.getName() );
				request.setAttribute("item_detail",idb.getDetail() );
				request.setAttribute("item_price",idb.getPrice() );
				request.setAttribute("item_fileName",idb.getFileName() );
				request.setAttribute("id",idb.getId() );

		// フォワードだよ
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemMasterupdate.jsp");
				dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");

		String inputItemName = request.getParameter("item_name");
		String inputItemPrice = request.getParameter("item_price");
		String inputItemDetail = request.getParameter("item_detail");
		String inputItemId = request.getParameter("id");

		  Part part = request.getPart("item_fileName");
		  String name = this.getFileName(part);
		  part.write(name);

		  ItemDataBeans idb = new ItemDataBeans();
			idb.setName(inputItemName);
			idb.setPrice(Integer.parseInt(inputItemPrice));
			idb.setDetail(inputItemDetail);
			idb.setFileName(name);
			idb.setId(Integer.parseInt(inputItemId));

			request.setAttribute("idb", idb);


			// フォワードだよ
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemMasterupdateconfirm.jsp");
			dispatcher.forward(request, response);

	}

    private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
    }
}