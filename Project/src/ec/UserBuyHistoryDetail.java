package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import dao.BuyDao;
import dao.BuyDetailDao;

/**
 * Servlet implementation class UserBuyHistoryDetail
 */
@WebServlet("/UserBuyHistoryDetail")
public class UserBuyHistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserBuyHistoryDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
		// マルチバイト文字列の文字化け対応
		request.setCharacterEncoding("UTF-8");

		// URLパラメータから値を取得
		String buyID = request.getParameter("buy_id");
		int buyId = Integer.parseInt(buyID);


		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		BuyDao BuyDAO = new BuyDao();
		BuyDataBeans bdb = BuyDAO.getBuyDataBeansByBuyId(buyId);

		// リクエスト領域に値をセット
		request.setAttribute("bdb",bdb );

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行

		BuyDetailDao BuyDetailDAO = new BuyDetailDao();
		ArrayList<ItemDataBeans> bddb = BuyDetailDAO.PurchaseDetails(buyId);

		// リクエスト領域に値をセット
		request.setAttribute("bddb",bddb );

		// フォワードだよ
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userbuyhistorydetail.jsp");
				dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}
}
