<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<form>

		<header>


			<div class="bg-warning clearfix p-3">

				<strong> <a class="text-danger float-left btn-lg" href="Index" >週刊少年ジャンプECサイト</a>
				</strong>


				<ul class="nav justify-content-end">
					<nav class="nav">
						<a
							class="nav-link active" href="UserData?id=${userInfo.id}">ユーザー情報</a> <a class="nav-link"
							href="Cart">カート</a> <a class="nav-link" href="Logout">ログアウト</a>
					</nav>
				</ul>

			</div>

		</header>


		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>

		<div align="center">
			<h4>購入が完了しました</h4>
		</div>

		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>

		<div class="container">
			<div class="row justify-content-md-center"></div>
			<div class="row">
				<div class="col ">
					<a class="btn btn-primary" href="Index" role="button"> 引き続き買い物をする </a>
				</div>
				<div class="col-md-auto"></div>
				<div class="col col-lg-3">
					<a class="btn btn-primary" href="UserData?id=${userInfo.id}" role="button"> ユーザー情報へ </a>
				</div>
			</div>
		</div>

		<div align="center">
			<h4>購入詳細</h4>
		</div>

		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-8">
				<div class="form-group">
					<div class="form-row">
						<table class="table shadow p-3 mb-5 bg-light rounded">
							<tr>
								<td class="center"></td>
								<td class="center">購入日時</td>
								<td class="center"></td>
								<td class="center">配送方法</td>
								<td class="center">合計金額</td>
							</tr>
							<tr>
								<td></td>
								<td>${resultBDB.formatDate}</td>
								<td></td>
								<td class="center">${resultBDB.deliveryMethodName}</td>
								<td class="center">${resultBDB.totalPrice}円</td>
							</tr>

						</table>

					</div>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-8">
				<div class="form-group">
					<div class="form-row">
						<table class="table shadow p-3 mb-5 bg-light rounded">
							<tr>
								<td class="center"></td>
								<td class="center">商品名</td>
								<td class="center"></td>
								<td class="center"></td>
								<td class="center">単価</td>
							</tr>
							<c:forEach var="buyIDB" items="${buyIDBList}" >
							<tr>
								<td></td>
								<td>${buyIDB.name}</td>
								<td></td>
								<td class="center"></td>
								<td class="center">${buyIDB.formatPrice}円</td>
							</tr>
							</c:forEach>
							<tr>
								<td></td>
								<td>${resultBDB.deliveryMethodName}</td>
								<td></td>
								<td class="center"></td>
								<td class="center">${resultBDB.deliveryMethodPrice}円</td>
							</tr>
							<tr></tr>
						</table>

					</div>
				</div>
			</div>
		</div>

		<footer>

			<p class="bg-warning text-white rounded p-4"></p>

		</footer>

	</form>
</body>
</html>
