<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>アイテム新規登録/確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<header>


		<div class="bg-warning clearfix p-3">

			<strong> <a class="text-danger float-left btn-lg" href="#">週刊少年ジャンプECサイト</a>
			</strong>


			<ul class="nav justify-content-end">
				<nav class="nav"></nav>
			</ul>

		</div>

	</header>

	<div class="row justify-content-center">
		<div class="col-4">
			<div class="form-group">
				<div class="form-row"></div>
			</div>
		</div>
	</div>

	<form action="ItemMasterRegistResult" method="POST">

		<div align="center">
			<h2>アイテム新規登録確認</h2>
		</div>

		<div class="row justify-content-center">
			<div class="col-4">
				<div class="form-group">
					<div class="form-row"></div>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-2">
				<div class="form-group">
					<div class="form-row">
						<label for="inputimg">画像</label>
						<div class="input-field col s10 offset-s1">
							<input type="text" name="file" value="${idb.fileName}" readonly>
							<img src="img/${idb.fileName}" class="card-img-top" alt="カードの画像">

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-2">
				<div class="form-group">
					<div class="form-row">
						<label for="inputitemname">商品名</label>
						<div class="input-field col s10 offset-s1">
							<input type="text" name="item_name" value="${idb.name}" readonly>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-2">
				<div class="form-group">
					<div class="form-row">
						<label for="inputitemdetails">値段</label>
						<div class="input-field col s10 offset-s1">
							<input type="text" name="item_price" value="${idb.price}"
								readonly>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-2">
				<div class="form-group">
					<div class="form-row">
						<label for="inputitemdetails">商品詳細</label>
						<div class="input-field col s10 offset-s1">
							<input type="text" name="item_detail" value="${idb.detail}"
								readonly>
						</div>
					</div>
				</div>
			</div>
		</div>



		<div class="row justify-content-center">
			<div class="col-4">
				<div class="form-group">
					<div class="form-row"></div>
				</div>
			</div>
		</div>


		<div align="center">
			<h5>上記の内容で登録してもよろしいでしょうか？</h5>
		</div>

		<div class="row justify-content-center">
			<div class="col-4">
				<div class="form-group">
					<div class="form-row"></div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row justify-content-md-center"></div>
			<div class="row">
				<div class="col "></div>
				<div class="col-md-3">
					<a class="btn btn-danger" href="#" role="button"> 修正 </a>
				</div>
				<div class="col col-lg-5">
					<button type="submit" class="btn btn-danger">登録</button>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-4">
				<div class="form-group">
					<div class="form-row"></div>
				</div>
			</div>
		</div>

		<footer>

			<p class="bg-warning text-white rounded p-4"></p>

		</footer>



	</form>
</body>
</html>






