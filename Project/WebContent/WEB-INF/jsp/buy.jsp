<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>


	<header>

		<div class="bg-warning clearfix p-3">

			<strong> <a class="text-danger float-left btn-lg" href="Index" >週刊少年ジャンプECサイト</a>
			</strong>


			<ul class="nav justify-content-end">
				<nav class="nav">
					<a class="nav-link active" href="#">ユーザー情報</a> <a class="nav-link"
						href="#">カート</a> <a class="nav-link" href="#">ログアウト</a>
				</nav>
			</ul>

		</div>

	</header>



	<div class="row justify-content-center">
		<div class="form-row">
			<div class="form-group">
				<label for="inputbday"> </label>

			</div>
		</div>
	</div>

	<div align="center">
		<h1>購入</h1>
	</div>

	<div class="row justify-content-center">
		<div class="form-row">
			<div class="form-group">
				<label for="inputbday"> </label>
			</div>
		</div>
	</div>

	<form action="BuyConfirm" method="POST">

		<div class="row justify-content-center">
			<div class="col-8">
				<div class="form-group">
					<div class="form-row">
						<table class="table shadow p-3 mb-5 bg-light rounded">
							<tr>
								<td class="center">商品名</td>
								<td class="center">単価</td>
								<td class="center">小計</td>
							</tr>
							<c:forEach var="cartInItem" items="${cart}">
								<tr>
									<td>${cartInItem.name}</td>
									<td class="center">${cartInItem.price}円</td>
									<td class="center">${cartInItem.price}円</td>
								</tr>
							</c:forEach>
							<tr>
								<td class="center"></td>
								<td class="center"></td>
								<td class="center">
									<div class="input-field col s8 offset-s2 ">
										<select name="delivery_method_id">
											<c:forEach var="dmdb" items="${dmdbList}">
												<option value="${dmdb.id}">${dmdb.name}</option>
											</c:forEach>
										</select> <label>配送方法</label>
									</div>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<div class="row justify-content-center">
										<div class="form-group">
											<div class="form-row">
												<button type="submit" class="btn btn-danger " name="action">
													購入確認</button>
											</div>
										</div>
									</div>
								</td>
								<td></td>

							</tr>
						</table>

					</div>
				</div>
			</div>
		</div>
	</form>

	<footer>
		<p class="bg-warning text-white rounded p-4"></p>
	</footer>




</body>
</html>
