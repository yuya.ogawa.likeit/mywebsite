<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報/更新確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<form action="UserDataUpdateResult" method="POST">
		<input type="hidden" name="id" class="form-control" id="inputUserId"
			value="${id}">
		<header>


			<div class="bg-warning clearfix p-3">

				<strong> <a class="text-danger float-left btn-lg" href="#"　>週刊少年ジャンプECサイト</a>
				</strong>


				<ul class="nav justify-content-end">
					<nav class="nav">
						<a class="nav-link active" href="UserData?id=${userInfo.id}">ユーザー情報</a> <a class="nav-link"
							href="Cart">カート</a> <a class="nav-link" href="Logout">ログアウト</a>
					</nav>
				</ul>

			</div>

		</header>

		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>

		<div align="center">
			<h4>入力内容確認</h4>
		</div>

		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>


		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>


		<div class="row justify-content-center">
			<div class="col-8">
				<div class="form-group">
					<div class="form-row">
						<table class="table shadow p-3 mb-5 bg-light rounded">
							<tr>
								<td class="center"></td>
								<td class="center">名前
									<div class="input-field col s10 offset-s1">
										<input type="text" name="user_name" value="${udb.name}"
											readonly>
									</div>
								</td>
								<td class="center"></td>
								<td class="center"></td>
								<td class="center">ログインID

									<div class="input-field col s10 offset-s1">
										<input type="text" name="login_id" value="${udb.loginId}"
											readonly>
									</div>

								</td>
							</tr>
							<tr>
								<td></td>
								<td>住所
									<div class="input-field col s10 offset-s1">
										<input type="text" name="user_address" value="${udb.address}"
											readonly>
									</div>
								</td>
								<td></td>
								<td class="center"></td>
								<td class="center"></td>
							</tr>


						</table>

					</div>
				</div>
			</div>
		</div>

		<div align="center">
			<h6>上記の内容で更新してもよろしいでしょうか？</h6>
		</div>

		<div class="row justify-content-center">
			<div class="col-4">
				<div class="form-group">
					<div class="form-row"></div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row justify-content-md-center"></div>
			<div class="row">
				<div class="col "></div>
				<div class="col-md-3">
					<a class="btn btn-danger" href="UserData?id=${userInfo.id}" role="button"> 戻る </a>
				</div>
				<div class="col col-lg-5">
					<button type="submit" class="btn btn-danger">更新</button>
				</div>
			</div>
		</div>


		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>




		<footer>

			<p class="bg-warning text-white rounded p-4"></p>

		</footer>

	</form>
</body>
</html>
