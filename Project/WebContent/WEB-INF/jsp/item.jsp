<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<form action="ItemAdd" method="POST">
		<input type="hidden" name="item_id" value="${idb.id}">

		<header>


			<div class="bg-warning clearfix p-3">

				<strong> <a class="text-danger float-left btn-lg"
					href="Index">週刊少年ジャンプECサイト</a>
				</strong>


				<ul class="nav justify-content-end">
					<nav class="nav">
						<a class="nav-link active" href="UserData?id=${userInfo.id}">ユーザー情報</a>
						<a class="nav-link" href="Cart">カート</a> <a class="nav-link"
							href="Logout">ログアウト</a>
					</nav>
				</ul>

			</div>

		</header>

		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>

		<div align="center">
			<h4>商品詳細</h4>
		</div>

		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>

		<div align="center">
			<button type="submit" class="btn btn-danger">カートに追加</button>
		</div>


		<div align="center">
			<h4></h4>
		</div>

		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group"></div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-1">
				<div class="form-group">
					<div class="form-row">
						<div align="center">
							<h3>${idb.name}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-2">
				<div class="form-group">
					<div class="form-row">

						<img src="img/${idb.fileName}" class="card-img-top" alt="カードの画像">

					</div>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-2">
				<div class="form-group">
					<div class="form-row">
						<label for="inputitemdetails">値段</label> <br> ${idb.price}円
					</div>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-2">
				<div class="form-group">
					<div class="form-row">
						<label for="inputitemdetails">商品詳細</label> <br> ${idb.detail}
					</div>
				</div>
			</div>
		</div>



		<footer>

			<p class="bg-warning text-white rounded p-4"></p>

		</footer>

	</form>
</body>
</html>
