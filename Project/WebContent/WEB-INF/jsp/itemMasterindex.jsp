<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
        <title>アイテムマスタ</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<form>

    <header>

        <div class="bg-warning clearfix p-3">

           <strong>
            <a class="text-danger float-left btn-lg" href="Index" >週刊少年ジャンプECサイト</a>
            </strong>


            <ul class="nav justify-content-end">
            <nav class="nav">

</nav>
            </ul>

　　　　　</div>

    </header>

      <div class="row justify-content-center">
        <div class="form-row">
        <div class="form-group">
            <label for="inputbday">　　</label>
        </div>
        </div>
        </div>

     <div align="center">
        <h1><font color="red" bgcolor="black"> 管理者画面 </font></h1>
    </div>

    <div class="row justify-content-center">
        <div class="form-row">
        <div class="form-group">
            <label for="inputbday">　　</label>
        </div>
        </div>
        </div>

    <div align="center">
        <h1><font color="red" bgcolor="black"> 週刊少年ジャンプECサイト </font></h1>
    </div>

     <div class="row justify-content-center">
        <div class="form-row">
        <div class="form-group">
            <label for="inputbday">　　</label>

        </div>
        </div>
        </div>


        <div class="row justify-content-center">
        <div class="form-group">
        <div class="form-row">
           <a class="btn btn-danger" href="ItemMasterRegist" role="button">   アイテム登録   </a>
        </div>
        </div>
        </div>


    <div align="center">
        <h4></h4>
    </div>


<c:forEach var="item" items="${itemList}" >

    <div class="row justify-content-center">
        <div class="form-row">
        <div class="form-group">


      <div class="container">
  <div class="row row-cols-3">
    <div class="col">


        <div class="card" style="width: 30rem;">
  <img src="img/${item.fileName}" class="card-img-top" alt="${item.fileName}">
  <div class="card-body">
    <h5 class="card-title">${item.name}</h5>
    <p class="card-text">${item.detail}</p>

    <p class="card-text">${item.price}円</p>
      <a href="ItemMasterUpdate?id=${item.id}" class="btn btn-danger">更新</a>
     <a href="ItemMasterDetails?id=${item.id}" class="btn btn-danger">詳細</a>
      <a href="ItemMasterDelete?id=${item.id}" class="btn btn-danger">削除</a>
  </div>
</div>

      </div>

  </div>
</div>

        </div>
        </div>
        </div>
</c:forEach>


    <footer>

           <p class="bg-warning text-white rounded p-4"></p>

            </footer>

</form>
</body>
</html>
