<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
        <title>購入確認</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<form action="BuyResult" method="post">

     <header>


        <div class="bg-warning clearfix p-3">

           <strong>
            <a class="text-danger float-left btn-lg" href="Index" >週刊少年ジャンプECサイト</a>
            </strong>


            <ul class="nav justify-content-end">
            <nav class="nav">
  <a class="nav-link active" href="UserData?id=${userInfo.id}">ユーザー情報</a>
  <a class="nav-link" href="Cart">カート</a>
  <a class="nav-link" href="Logout">ログアウト</a>
</nav>
            </ul>

　　　　　</div>

    </header>

    <div class="row justify-content-center">
        <div class="form-row">
        <div class="form-group">
            <label for="inputbday">　　</label>

        </div>
        </div>
        </div>

    <div align="center">
        <h1>　購入確認　</h1>
    </div>

        <div class="row justify-content-center">
        <div class="form-row">
        <div class="form-group">
            <label for="inputbday">　　</label>

        </div>
        </div>
        </div>


        <div class="row justify-content-center">
        <div class="col-8">
        <div class="form-group">
        <div class="form-row">
        <table class="table shadow p-3 mb-5 bg-light rounded">
             <tr>
                <td class="center"> 商品名</td>
                <td class="center">単価</td>
                <td class="center">小計</td>
            </tr>
             <c:forEach var="cartInItem" items="${cart}" >
            <tr>
                <td>${cartInItem.name}</td>
                <td class="center"> ${cartInItem.price}円</td>
                <td class="center"> ${cartInItem.price}円</td>
            </tr>
              </c:forEach>
            <tr>

										<td class="center">${bdb.deliveryMethodName}</td>
										<td class="center"></td>
										<td class="center">${bdb.deliveryMethodPrice}円</td>
									</tr>
									<tr>
										<td class="center"></td>
										<td class="center">合計</td>
										<td class="center">${bdb.totalPrice}円</td>
									</tr>

            <tr>
                <td></td>
                <td></td>
                <td class="center">
                    <button type="submit" class="btn btn-danger ">　　　 　　購入　　　　　　</button>
                </td>
                <td class="center"></td>
                <td class="center"></td>
            </tr>

        </table>

        </div>
        </div>
        </div>
        </div>

    <footer>

           <p class="bg-warning text-white rounded p-4"></p>

            </footer>

</form>
</body>
</html>
