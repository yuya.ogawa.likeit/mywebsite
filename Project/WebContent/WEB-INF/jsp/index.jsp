<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Index</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>


		<header>


			<div class="bg-warning clearfix p-3">

				<strong> <a class="text-danger float-left btn-lg" href="Index">週刊少年ジャンプECサイト</a>
				</strong>


				<ul class="nav justify-content-end">
					<nav class="nav">
						<a class="flex-sm-fill text-sm-center nav-link disabled" href="#"
							tabindex="-1" aria-disabled="true"> ${userInfo.name} さん</a> <a
							class="nav-link active" href="UserData?id=${userInfo.id}">ユーザー情報</a>
						<a class="nav-link" href="Cart">カート</a> <a class="nav-link"
							href="Logout">ログアウト</a> <a class="nav-link" href="Regist">新規登録</a>

							 <c:if test="${userInfo.name == '管理者'}">
						<a class="nav-link" href="ItemMasterIndex">管理者画面</a>
						</c:if>

						<a class="nav-link" href="Login">ログイン</a>
					</nav>
				</ul>

			</div>

		</header>

		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>

		<div align="center">
			<h1>
				<font color="red" bgcolor="black"> 週刊少年ジャンプECサイト </font>
			</h1>
			<h8> </h8>
		</div>

		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>

		<form action="ItemSearchResult" method="POST">

		<div class="row justify-content-center">
			<div class="col-4">
				<div class="form-group">
					<div class="form-row">
						<input type="text" name="name" class="form-control" id="inputSearch" placeholder="Search">
					</div>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="form-group">
				<div class="form-row">
					<button type="submit" class="btn btn-danger">検索</button>
				</div>
			</div>
		</div>



		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>


		<div align="center">
			<h4></h4>
		</div>

		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group"></div>
			</div>
		</div>


		<c:forEach var="item" items="${itemList}">

			<div class="row justify-content-center">
				<div class="form-row">
					<div class="form-group">


						<div class="container">
							<div class="row row-cols-3">
								<div class="col">


									<div class="card" style="width: 20rem;">
										<img src="img/${item.fileName}" class="card-img-top"
											alt="${item.fileName}">
										<div class="card-body">
											<h5 class="card-title">${item.name}</h5>
											<p class="card-text">${item.detail}</p>

											<p class="card-text">${item.price}円</p>

											<a href="ItemDetails?id=${item.id}" class="btn btn-danger">詳細</a>

										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</c:forEach>

		<footer>

			<p class="bg-warning text-white rounded p-4"></p>

		</footer>

	</form>
</body>
</html>
