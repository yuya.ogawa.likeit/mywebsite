<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>カート</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<header>
		<div class="bg-warning clearfix p-3">
			<strong> <a class="text-danger float-left btn-lg"
				href="Index">週刊少年ジャンプECサイト</a>
			</strong>

			<ul class="nav justify-content-end">
				<nav class="nav">
					<a class="flex-sm-fill text-sm-center nav-link disabled" href="#"
						tabindex="-1" aria-disabled="true"> ${userInfo.name} さん</a> <a
						class="nav-link active" href="UserData?id=${userInfo.id}">ユーザー情報</a><a
						class="nav-link" href="Logout">ログアウト</a>
				</nav>
			</ul>
		</div>
	</header>

	<div class="row justify-content-center">
		<div class="form-row">
			<div class="form-group">
				<label for="inputbday"> </label>
			</div>
		</div>
	</div>

	<div align="center">
		${cartActionMessage}
		<h3>カート</h3>
	</div>
	<form action="ItemDelete" method="POST">
		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row justify-content-md-center"></div>
			<div class="row">
				<div class="col ">
					<button type="submit" class="btn btn-danger">選択されている商品の削除</button>
				</div>
				<div class="col-md-auto"></div>
				<div class="col col-lg-3">
					<a class="btn btn-primary" href="Buy" role="button"> レジに進む 💲</a>
				</div>
			</div>
		</div>


		<div align="center">
			<h4></h4>
		</div>

		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group"></div>
			</div>
		</div>
		<c:forEach var="item" items="${cart}" varStatus="status">
			<div class="row justify-content-center">
				<div class="form-row">
					<div class="form-group">


						<div class="container">
							<div class="row row-cols-3">
								<div class="col">

									<div class="card" style="width: 30rem;">
										<img src="img/${item.fileName}" class="card-img-top"
											alt="カードの画像">
										<div class="card-body">
											<h5 class="card-title">${item.name}</h5>
											<p class="card-text">${item.price}円</p>

											<div class="custom-control custom-checkbox">
												<input type="checkbox"
													class="custom-control-input your-checkbox"
													id="${status.index}" name="delete_item_id_list"
													value="${item.id}"> <label
													class="custom-control-label" for="${status.index}">削除</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</c:forEach>

		<footer>

			<p class="bg-warning text-white rounded p-4"></p>

		</footer>

	</form>
</body>
</html>
