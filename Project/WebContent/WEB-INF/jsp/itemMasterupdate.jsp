<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>アイテム更新</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
</head>
	<body>

        <header>


        <div class="bg-warning clearfix p-3">

           <strong>
            <a class="text-danger float-left btn-lg" href="#"　>週刊少年ジャンプECサイト</a>
            </strong>


            <ul class="nav justify-content-end">
            <nav class="nav">

</nav>
            </ul>

　　　　　</div>

    </header>

        <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">

            </div>
            </div>
            </div>
            </div>

		<form action="ItemMasterUpdate" method="POST" enctype="multipart/form-data">

		<input type="hidden" name="id" class="form-control" id="inputUserId" value="${id}">

            <div align="center">
            <h2>アイテム更新</h2>
            </div>

            <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">
                 <label for="inputimg">画像</label>
                 <div class="input-field col s10 offset-s1">
				 <img src="img/${item_fileName}" class="card-img-top" alt="カードの画像">
                 <input type="file" name="item_fileName" class="form-control" id="inputitem_fileName" value="${item_fileName}">
                 </div>
            </div>
            </div>
            </div>
            </div>

             <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">
                <label for="inputitemname">商品名</label>
                <input type="text" name="item_name" class="form-control" id="inputItemName" value="${item_name}">

            </div>
            </div>
            </div>
            </div>

             <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">
                <label for="inputitemdetails">値段</label>
                <input type="text" name="item_price" class="form-control" id="inputitem_price" value="${item_price}">
            </div>
            </div>
            </div>
            </div>

             <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">
                <label for="inputitemdetails">商品詳細</label>
                <input type="text" name="item_detail" class="form-control" id="inputitem_detail" value="${item_detail}">
            </div>
            </div>
            </div>
            </div>




            <div align="center">
                 <button type="submit" class="btn btn-danger">　更新　</button>
            </div>

           <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">

            </div>
            </div>
            </div>
            </div>

            <footer>

           <p class="bg-warning text-white rounded p-4"></p>

            </footer>



</form>
	</body>
</html>






