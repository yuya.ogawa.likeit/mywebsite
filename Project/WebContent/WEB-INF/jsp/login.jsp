<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<body>


	<header>
		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<div class="bg-warning clearfix p-3">

			<strong> <a
				class="text-danger float-left btn-lg text-decoration-none"
				href="Index">週刊少年ジャンプECサイト</a>
			</strong>


			<ul class="nav justify-content-end">
				<nav class="nav">

					<a class="nav-link" href="Regist">新規登録</a>
				</nav>
			</ul>
		</div>

	</header>
	<form action="Login" method="POST">

		<div class="row justify-content-center">
			<div class="col-4">
				<div class="form-group">
					<div class="form-row"></div>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-4">
				<div class="form-group">
					<div class="form-row">
						<label for="exampleInputLogin">ログイン ID</label> <input type="text"
							class="form-control" name="loginId" placeholder="login">
					</div>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-4">
				<div class="form-group">
					<div class="form-row">
						<label for="exampleInputPassword1">パスワード</label> <input
							type="password" name="password" class="form-control"
							placeholder="Password">
					</div>
				</div>
			</div>
		</div>

		<div align="center">
			<button type="submit" class="btn btn-danger" name="action">ログイン</button>
		</div>

		<div class="row justify-content-center">
			<div class="col-4">
				<div class="form-group">
					<div class="form-row"></div>
				</div>
			</div>
		</div>

	</form>

	<footer>

		<p class="bg-warning text-white rounded p-4"></p>

	</footer>

</body>
</html>


