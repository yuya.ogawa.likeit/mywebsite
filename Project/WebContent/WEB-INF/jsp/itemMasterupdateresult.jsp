<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>アイテム更新/完了</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
</head>
	<body>

       <header>


        <div class="bg-warning clearfix p-3">

           <strong>
            <a class="text-danger float-left btn-lg" href="#"　>週刊少年ジャンプECサイト</a>
            </strong>


            <ul class="nav justify-content-end">
            <nav class="nav">

</nav>
            </ul>

　　　　　</div>

    </header>

        <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">

            </div>
            </div>
            </div>
            </div>

		<form>

            <div class="row justify-content-center">
            <div class="col-2">
            <div class="form-group">
            <div class="form-row">
                 <label for="inputimg">画像</label>
                 <br>　　　 <img src="img/${idb.fileName}" class="card-img-top" alt="カードの画像">
            </div>
            </div>
            </div>
            </div>

             <div class="row justify-content-center">
            <div class="col-2">
            <div class="form-group">
            <div class="form-row">
                <label for="inputitemname">商品名</label>
                <br>　　${idb.name}
            </div>
            </div>
            </div>
            </div>

             <div class="row justify-content-center">
            <div class="col-2">
            <div class="form-group">
            <div class="form-row">
                <label for="inputitemname">値段</label>
                <br>　　${idb.price}
            </div>
            </div>
            </div>
            </div>

             <div class="row justify-content-center">
            <div class="col-2">
            <div class="form-group">
            <div class="form-row">
                <label for="inputitemdetails">商品詳細</label>
                <br>　${idb.detail}
            </div>
            </div>
            </div>
            </div>



             <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">

            </div>
            </div>
            </div>
            </div>


              <div align="center">
            <h6>上記の内容で更新しました</h6>
            </div>

             <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">

            </div>
            </div>
            </div>
            </div>



    <div align="center">
         <div class="col-md-3">
      <a class="btn btn-danger" href="ItemMasterIndex" role="button"> アイテムマスタ画面へ </a>
    </div>

            <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">

            </div>
            </div>
            </div>
            </div>

            <footer>

           <p class="bg-warning text-white rounded p-4"></p>

            </footer>



</form>
	</body>
</html>






