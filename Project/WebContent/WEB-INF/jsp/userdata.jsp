<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<form action="UserDataUpdateConfirm" method="POST">

		<header>


			<div class="bg-warning clearfix p-3">

				<strong> <a class="text-danger float-left btn-lg"
					href="Index">週刊少年ジャンプECサイト</a>
				</strong>


				<ul class="nav justify-content-end">
					<nav class="nav">
						<a class="flex-sm-fill text-sm-center nav-link disabled" href="#"
							tabindex="-1" aria-disabled="true"> ${userInfo.name} さん</a> <a
							class="nav-link" href="Cart">カート</a> <a class="nav-link" href="Logout">ログアウト</a>
					</nav>
				</ul>

			</div>

		</header>

		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>

		<div align="center">
			<h4>ユーザー情報</h4>
		</div>

		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>


		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>


		<div class="row justify-content-center">
			<div class="col-8">
				<div class="form-group">
					<div class="form-row">
						<table class="table shadow p-3 mb-5 bg-light rounded">
							<tr>
								<td class="center"></td>
								<td class="center">
									<div class="input-field col s6">
										名前 <input type="text" name="user_name" value="${udb.name}">
									</div>
								</td>
								<td class="center"></td>
								<td class="center"></td>
								<td class="center">
									<div class="input-field col s6">
										ログインID <input type="text" name="login_id"
											value="${udb.loginId}">
									</div>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<div class="input-field col s6">
										住所 <input type="text" name="user_address"
											value="${udb.address}">
									</div>
								</td>
								<td></td>
								<td class="center"></td>
								<td class="center"></td>
							</tr>


						</table>

					</div>
				</div>
			</div>
		</div>

		<div align="center" class="shadow-lg">
			<button type="submit" class="btn btn-danger ">更新</button>
		</div>

		<div class="row justify-content-center">
			<div class="form-row">
				<div class="form-group">
					<label for="inputbday"> </label>

				</div>
			</div>
		</div>


		<div class="row justify-content-center">
			<div class="col-8">
				<div class="form-group">
					<div class="form-row">
						<table class="table shadow　table-bordered">
							<tr>
								<td>購入日時</td>
								<td>配送方法</td>
								<td>購入金額</td>
								<td></td>
							</tr>
							<c:forEach var="bdb" items="${dbdList}">
								<tr>
									<td>${bdb.formatDate}</td>
									<td>${bdb.deliveryMethodName}</td>
									<td>${bdb.totalPrice}円</td>
									<td>
										<div align="center">
											<a class="btn btn-danger"
												href="UserBuyHistoryDetail?buy_id=${bdb.id}" role="button">
												購入情報詳細 </a>
										</div>
									</td>

								</tr>
							</c:forEach>
						</table>

					</div>
				</div>
			</div>
		</div>

		<footer>

			<p class="bg-warning text-white rounded p-4"></p>

		</footer>

	</form>
</body>
</html>
